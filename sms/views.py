# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import africastalking
from django.shortcuts import render

# Create your views here.

def send_sms(request):
    username = "YOUR_USERNAME"  # use 'sandbox' for development in the test environment
    api_key = "YOUR_API_KEY"  # use your sandbox app API key for development in the test environment
    africastalking.initialize(username, api_key)

    # Initialize a service e.g. SMS
    sms = africastalking.SMS
    response = sms.send("Hello Message!", ["+2547xxxxxx"])
    print(response)