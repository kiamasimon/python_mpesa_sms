from django.conf.urls import url

from . import views

app_name = 'c2b'
#
urlpatterns = [
    url(r'^c2b/access/token', views.getAccessToken, name='get_mpesa_access_token'),
    url(r'^c2b/register', views.register_urls, name="register_mpesa_validation"),
    url(r'^c2b/confirmation', views.confirmation, name="confirmation"),
    url(r'^c2b/validation', views.validation, name="validation"),
    url(r'^c2b/callback', views.call_back2, name="call_back"),
    url(r'^c2b/stk_push', views.stk_push, name='stk_push')
]