# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import base64
from datetime import time
from time import gmtime

from django.shortcuts import render

# Create your views here.
from django.utils.datetime_safe import strftime

from . mpesa_credentials import MpesaC2bCredential, MpesaAccessToken
import requests
from requests.auth import HTTPBasicAuth
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from mpesa_api.models import MpesaPayment, MpesaCallBacks
import json


@csrf_exempt
def getAccessToken(request):
    r = requests.get(MpesaC2bCredential.api_URL, auth=HTTPBasicAuth(MpesaC2bCredential.consumer_key, MpesaC2bCredential.consumer_secret))
    mpesa_access_token = json.loads(r.text)
    return HttpResponse(mpesa_access_token['access_token'])


@csrf_exempt
def register_urls(request):
    access_token = MpesaAccessToken.validated_mpesa_access_token
    # print(access_token)
    api_url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl"
    # api_url = "https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl"
    headers = {"Authorization": "Bearer %s" % access_token}
    options = {"ShortCode": "602982",
               "ResponseType": "Completed",
               "ConfirmationURL": "https://1da09d53.ngrok.io/api/v1/c2b/confirmation",
               "ValidationURL": "https://1da09d53.ngrok.io/api/v1/c2b/validation"}
    response = requests.post(api_url, json=options, headers=headers)
    return HttpResponse(response.text)


@csrf_exempt
def call_back(request):
    mpesa_body = request.body.decode('utf-8')
    mpesa_payment = json.loads(mpesa_body)
    print(mpesa_payment)
    json_dump = json.dumps(mpesa_payment)

    call_back_data = MpesaCallBacks(
        ip_address=mpesa_payment['ip'],
        caller='C2B Validation',
        conversation_id=mpesa_payment['TransID'],
        content=mpesa_payment.text,
    )

    call_back_data.save()
    return HttpResponse(mpesa_payment.text)


@csrf_exempt
def validation(request):
    pass


@csrf_exempt
def confirmation(request):
    mpesa_body =request.body.decode('utf-8')
    mpesa_payment = json.loads(mpesa_body)
    print(mpesa_payment)
    json_dump = json.dumps(mpesa_payment)

    payment = MpesaPayment(
        first_name=mpesa_payment['FirstName'],
        last_name=mpesa_payment['LastName'],
        description=mpesa_payment['MSISDN'],
        phone_number=mpesa_payment['MSISDN'],
        transaction_id=mpesa_payment['TransID'],
        amount=mpesa_payment['TransAmount'],
        reference=mpesa_payment['BillRefNumber'],
        type='Mpesa',

    )
    payment.save()

    return HttpResponse(json_dump['ResponseDescription'])


@csrf_exempt
def stk_push(request):
    access_token = MpesaAccessToken.validated_mpesa_access_token
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
    headers = {"Authorization": "Bearer %s" % access_token}
    pass_key = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"
    MerchantID = "174379"
    timestamp = "20190531130024"
    password = base64.b64encode(MerchantID + pass_key + timestamp)
    request = {
        "BusinessShortCode": "174379",
        "Password": password,
        "Timestamp": "20190531130024",
        "TransactionType": "CustomerPayBillOnline",
        "Amount": "1",
        "PartyA": "254704976963",
        "PartyB": "174379",
        "PhoneNumber": "254704976963",
        "CallBackURL": "https://1da09d53.ngrok.io/c2b/callback",
        "AccountReference": "account",
        "TransactionDesc": "STK PUSH EXAMPLE"
    }

    response = requests.post(api_url, json=request, headers=headers)
    print (response.text)
    return HttpResponse(response.text)


@csrf_exempt
def call_back2(request):
    mpesa_body = request.body.decode('utf-8')
    mpesa_payment = json.loads(mpesa_body)
    print(mpesa_payment)
    json_dump = json.dumps(mpesa_payment)

    call_back_data = MpesaCallBacks(
        ip_address=mpesa_payment['CheckoutRequestID'],
        caller='STK Validation',
        conversation_id=mpesa_payment['MpesaReceiptNumber'],
        content=mpesa_payment.text,
    )

    call_back_data.save()
    return HttpResponse(mpesa_payment.text)