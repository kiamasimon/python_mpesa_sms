import requests
import json
from requests.auth import HTTPBasicAuth


class MpesaC2bCredential:
    consumer_key = "v8yoh9JO76Ttp6fHrnofwpjork0iR3pw"
    consumer_secret = "Wg15IQOGJdOP92PO"
    # https://api.safaricom.co.ke/oauth/v1/generate
    api_URL = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"


class MpesaAccessToken:
    r = requests.get(MpesaC2bCredential.api_URL,
                     auth=HTTPBasicAuth(MpesaC2bCredential.consumer_key, MpesaC2bCredential.consumer_secret))
    mpesa_access_token = json.loads(r.text)
    validated_mpesa_access_token = mpesa_access_token['access_token']
